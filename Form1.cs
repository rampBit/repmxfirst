﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Exchange.WebServices.Data;
using Microsoft.Exchange.WebServices.Autodiscover;
using System.Net;
using System.IO;
using System.Net.Http;

namespace InterceptExchangeEmail
{
    public partial class Form1 : Form
    {
        string redactURL = "http://52.205.42.150:8234/rest/a8737f39-7f37-4ccf-95a4-f51c1830b54f/redact";
        ExchangeService exchange = null;
        List<EmailMessage> messages;
        public Form1()
        {
            InitializeComponent();
            lstMsg.Clear();
            lstMsg.View = View.Details;
            lstMsg.Columns.Add("Date", 100);
            lstMsg.Columns.Add("From", 150);
            lstMsg.Columns.Add("Subject", 300);
            lstMsg.Columns.Add("Has Attachment", 100);
            lstMsg.Columns.Add("Id", 100);
            lstMsg.FullRowSelect = true;
            //listEmailBody.Columns.Add("From", 100);
            //listEmailBody.Columns.Add("Receipients", 150);
            //listEmailBody.Columns.Add("Subject", 250);
            messages = new List<EmailMessage>();
        }

        private void btnRead_Click(object sender, EventArgs e)
        {
            lstMsg.Items.Clear();
            ConnectToExchangeServer();
            TimeSpan ts = new TimeSpan(0, -1, 0, 0);
            DateTime fromDate = DateTime.Now.AddDays(-10).Date;
            SearchFilter.IsGreaterThanOrEqualTo fromDateFilter = new SearchFilter.IsGreaterThanOrEqualTo(ItemSchema.DateTimeReceived, fromDate);

            DateTime toDate = DateTime.Now;
            SearchFilter.IsLessThan toDateFilter = new SearchFilter.IsLessThan(ItemSchema.DateTimeReceived, toDate);

            SearchFilter.SearchFilterCollection filters = new SearchFilter.SearchFilterCollection(LogicalOperator.And, fromDateFilter, toDateFilter);
            if (!string.IsNullOrWhiteSpace(FromFilterTxt.Text))
            {
                SearchFilter.ContainsSubstring fromFilter = new SearchFilter.ContainsSubstring(EmailMessageSchema.From, FromFilterTxt.Text);
                filters.Add(fromFilter);
            }

            if (exchange != null)
            {
                FindItemsResults<Item> findResults = exchange.FindItems(WellKnownFolderName.Inbox, filters, new ItemView(50));
                
                if (findResults.Items.Count > 0)
                {
                    bool ack = false;
                    foreach (Item item in findResults)
                    {

                        EmailMessage message = EmailMessage.Bind(exchange, item.Id);
                        messages.Add(message);
                        if (message.Body.ToString().Contains("Your decision is requested"))
                        {
                            ListViewItem listitem = new ListViewItem(new[]{
                        message.DateTimeReceived.ToString(), message.From.Name.ToString() + "(" + message.From.Address.ToString() + ")", message.Subject, ((message.HasAttachments) ? "Yes" : "No"), message.Id.ToString(),message.Body.ToString()
                        });
                            lstMsg.Items.Add(listitem);
                          ack = SendToTeraDact(message);
                            
                            var currentRules = exchange.GetInboxRules("rampuser@rampgroupUS.onmicrosoft.com");
                        }
                    }
                    if (ack)
                        lblackTeradact.Text = "Ackowledgment RCVD from Teradact API";
                    else
                        lblackTeradact.Text = "Failed to Get response from Teradact API";
                }
                if (findResults.Items.Count <= 0)
                {
                    lstMsg.Items.Add("No Messages found!!");
                }
            }
        }
        private static bool RedirectionUrlValidationCallback(string redirectionUrl)
        {
            // The default for the validation callback is to reject the URL.
            bool result = false;

            Uri redirectionUri = new Uri(redirectionUrl);

            // Validate the contents of the redirection URL. In this simple validation
            // callback, the redirection URL is considered valid if it is using HTTPS
            // to encrypt the authentication credentials. 
            if (redirectionUri.Scheme == "https")
            {
                result = true;
            }
            return result;

        }

        public void ConnectToExchangeServer()
        {

            lblMsg.Text = "Connecting to Exchange Server..";
            lblMsg.Refresh();
            try
            {
                exchange = new ExchangeService(ExchangeVersion.Exchange2010_SP2);
                exchange.Credentials = new WebCredentials("Rampusa@rampgroupUS.onmicrosoft.com", "ptg@1234", "rampgroupUS.onmicrosoft.com");
                //  hemanth.potluri@CodeSmartInc.com", "Kumar9966$", "CodeSmartInc.com");
                exchange.TraceEnabled = true;
                //exchange.AutodiscoverUrl("Rampusa@rampgroupUS.onmicrosoft.com");
                // //https://outlook.office.com/EWS/Exchange.asmx");

                exchange.AutodiscoverUrl(
    "Rampusa@rampgroupUS.onmicrosoft.com",
    delegate (string url)
    {
        return true;
    });



                lblMsg.Text = "Connected to Exchange Server : " + exchange.Url.Host;
                lblMsg.Refresh();

            }
            catch (Exception ex)
            {
                lblMsg.Text = "Error Connecting to Exchange Server!!" + ex.Message;
                lblMsg.Refresh();
            }
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            if (exchange != null)
            {
                if (lstMsg.Items.Count > 0)
                {
                    ListViewItem item = lstMsg.SelectedItems[0];

                    if (item != null)
                    {
                        string msgid = item.SubItems[4].Text.ToString();
                        EmailMessage message = EmailMessage.Bind(exchange, new ItemId(msgid));
                        if (message.HasAttachments && message.Attachments[0] is FileAttachment)
                        {
                            FileAttachment fileAttachment = message.Attachments[0] as FileAttachment;
                            //Change the below Path    
                            fileAttachment.Load(@"D:\\EOP\\InterceptExchangeEmail\\Attachments\" + fileAttachment.Name);
                            lblAttach.Text = "Attachment Downloaded : " + fileAttachment.Name;
                        }
                        else
                        {
                            MessageBox.Show("No Attachments found!!");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Please select a Message!!");
                    }
                }
                else
                {
                    MessageBox.Show("Messages not loaded!!");
                }

            }
            else
            {
                MessageBox.Show("Not Connected to Mail Server!!");
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void lstMsg_Click(object sender, EventArgs e)
        {
            mailContent.DocumentText = messages[lstMsg.SelectedIndices[0]].Body.Text;
        }

        public bool  SendToTeraDact(EmailMessage itm)
        {   
            byte[] Originalstream = Encoding.ASCII.GetBytes(itm.Body.ToString());
            //byte[] Originalstream = File.ReadAllBytes(@"C:\Redaction_Documents\arrestReports.docx");
            FileStream rulesetStream = new FileStream(@"C:\Redaction_Documents\SSN.txt", FileMode.Open);
            var result = Upload(redactURL, Originalstream, rulesetStream);
            rulesetStream.Close();
            if (result != null)
            {
                using (var fileStream = new FileStream(@"C:\TeradactResponseDoc\redacted.docx", FileMode.Create, FileAccess.Write))
                {
                    result.CopyTo(fileStream);
                    return true;
                }
            }
            else
                return false;
        }

       
        private Stream Upload(string url, byte[] fileStream, Stream fileBytes)
        {
            using (var client = new HttpClient())
            {
                var multiPartContent = new MultipartFormDataContent();
                var fileStreamContent = new ByteArrayContent(fileStream);
                fileStreamContent.Headers.Add("Content-Type", "application/octet-stream");
                fileStreamContent.Headers.Add("Content-Transfer-Encoding", "binary");
                multiPartContent.Add(fileStreamContent, "doc");
                var textContent = new StreamContent(fileBytes);
                textContent.Headers.Add("Content-Type", "text/plain");
                textContent.Headers.Add("Content-Transfer-Encoding", "8bit");
                multiPartContent.Add(textContent, "trs");
                url = url + "?rulesetAttached=true";
                var request = new HttpRequestMessage(HttpMethod.Put, url);
                request.Content = multiPartContent;

                HttpResponseMessage response = client.SendAsync(request).Result;
               
                #region Log Request
                //Stream stream;
                //byte[] buffer;
                //stream = response.Content.ReadAsStreamAsync();

                //buffer = new byte[1000];
                //int byteCount = await stream.ReadAsync(buffer, 0, buffer.Length);
                //request.Abort();  // call ASAP to kill connection          
                //response.Close();
                //var responsefrLog = client.GetAsync(url,HttpCompletionOption.ResponseHeadersRead);

                ////var responselog = await client.GetAsync(request)
                using (var stsdream = response.Content.ReadAsStreamAsync())
                {
                    var bytes = new byte[1000];
                    var bytesread = stsdream.Result.Read(bytes, 0, 1000);
                    //var bytesread = stream.Read(bytes, 0, 1000);
                    //stsdream.Close();
                
                
                //var text = Encoding.UTF8.GetString(bytes);

                //    Stream receiveStream = response.Content.ReadAsStreamAsync();
                //    StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
                //    txtBlock.Text = readStream.ReadToEnd();
                }
                //using (var reader = new StreamReader(fileBytes))
                //{
                //    string headers = request.Headers.ToString();
                //    string body = reader.ReadToEnd();
                //    string rawRequest = string.Format(
                //        "{0}{1}{1}{2}", headers, Environment.NewLine, body
                //    );
                //    System.IO.File.WriteAllText(@"C:\TeradactApiLogs\LLog2.txt", rawRequest);

                //    //Console.WriteLine(rawRequest);
                //    //Console.ReadLine();
                //    // TODO: Log the rawRequest to your database
                //}
                #endregion
                if (!response.IsSuccessStatusCode)
                {
                   
                    return null;
                }

               
                return response.Content.ReadAsStreamAsync().Result;
            }
        }
    }
}
