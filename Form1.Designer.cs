﻿using System;

namespace InterceptExchangeEmail
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnRead = new System.Windows.Forms.Button();
            this.lstMsg = new System.Windows.Forms.ListView();
            this.btnLoad = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lblAttach = new System.Windows.Forms.Label();
            this.lblMsg = new System.Windows.Forms.Label();
            this.mailContent = new System.Windows.Forms.WebBrowser();
            this.FromFilterTxt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblackTeradact = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnRead
            // 
            this.btnRead.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRead.Location = new System.Drawing.Point(37, 11);
            this.btnRead.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnRead.Name = "btnRead";
            this.btnRead.Size = new System.Drawing.Size(125, 43);
            this.btnRead.TabIndex = 0;
            this.btnRead.Text = "Connect";
            this.btnRead.UseVisualStyleBackColor = true;
            this.btnRead.Click += new System.EventHandler(this.btnRead_Click);
            // 
            // lstMsg
            // 
            this.lstMsg.Location = new System.Drawing.Point(37, 100);
            this.lstMsg.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lstMsg.Name = "lstMsg";
            this.lstMsg.Size = new System.Drawing.Size(1348, 200);
            this.lstMsg.TabIndex = 1;
            this.lstMsg.UseCompatibleStateImageBehavior = false;
            this.lstMsg.Click += new System.EventHandler(this.lstMsg_Click);
            // 
            // btnLoad
            // 
            this.btnLoad.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLoad.Location = new System.Drawing.Point(37, 527);
            this.btnLoad.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(148, 37);
            this.btnLoad.TabIndex = 2;
            this.btnLoad.Text = "Load Attachment";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(41, 78);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(145, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Last Today\'s Email";
            // 
            // lblAttach
            // 
            this.lblAttach.AutoSize = true;
            this.lblAttach.Location = new System.Drawing.Point(229, 538);
            this.lblAttach.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAttach.Name = "lblAttach";
            this.lblAttach.Size = new System.Drawing.Size(183, 17);
            this.lblAttach.TabIndex = 4;
            this.lblAttach.Text = "No Attachment Downloaded";
            // 
            // lblMsg
            // 
            this.lblMsg.AutoSize = true;
            this.lblMsg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMsg.Location = new System.Drawing.Point(229, 25);
            this.lblMsg.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMsg.Name = "lblMsg";
            this.lblMsg.Size = new System.Drawing.Size(54, 17);
            this.lblMsg.TabIndex = 5;
            this.lblMsg.Text = "Status";
            this.lblMsg.Click += new System.EventHandler(this.label3_Click);
            // 
            // mailContent
            // 
            this.mailContent.Location = new System.Drawing.Point(37, 308);
            this.mailContent.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.mailContent.MinimumSize = new System.Drawing.Size(27, 25);
            this.mailContent.Name = "mailContent";
            this.mailContent.Size = new System.Drawing.Size(1349, 204);
            this.mailContent.TabIndex = 7;
            // 
            // FromFilterTxt
            // 
            this.FromFilterTxt.Location = new System.Drawing.Point(1147, 30);
            this.FromFilterTxt.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.FromFilterTxt.Name = "FromFilterTxt";
            this.FromFilterTxt.Size = new System.Drawing.Size(239, 22);
            this.FromFilterTxt.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(1029, 33);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 17);
            this.label2.TabIndex = 9;
            this.label2.Text = "Filter by From:";
            // 
            // lblackTeradact
            // 
            this.lblackTeradact.AutoSize = true;
            this.lblackTeradact.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblackTeradact.Location = new System.Drawing.Point(607, 537);
            this.lblackTeradact.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblackTeradact.Name = "lblackTeradact";
            this.lblackTeradact.Size = new System.Drawing.Size(0, 17);
            this.lblackTeradact.TabIndex = 10;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1425, 593);
            this.Controls.Add(this.lblackTeradact);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.FromFilterTxt);
            this.Controls.Add(this.mailContent);
            this.Controls.Add(this.lblMsg);
            this.Controls.Add(this.lblAttach);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnLoad);
            this.Controls.Add(this.lstMsg);
            this.Controls.Add(this.btnRead);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void label3_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        #endregion

        private System.Windows.Forms.Button btnRead;
        private System.Windows.Forms.ListView lstMsg;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblAttach;
        private System.Windows.Forms.Label lblMsg;
        private System.Windows.Forms.WebBrowser mailContent;
        private System.Windows.Forms.TextBox FromFilterTxt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblackTeradact;
    }
}

